import numpy as np 

#NUMPY-$2 ARRAY CREATİON (DİZİ OLUŞTURMA)

##############################################
a = np.array([2, 3, 4])
"""
	.array fonksiyonu ile ndarray olmayan bir
	değişkeni ndarray e dönüştürüyoruz.

"""

a.dtype
"""
	dtype('int64') ndarray içerisindeki
	veri tipini döndürür.
"""

##############################################


##############################################
b = np.array([1.2, 3.5, 5.1])

b.dtype
"""
	dtype('float64') değerini döndürür çünkü
	ndarray içerisinde flot(ondalıklı) sayı
	mevcuttur.
"""
##############################################


##############################################
c = np.array(['a', 'b', 'c'])
c.dtype
"""
	dtype('<U1') değerini döndürür.
"""

##############################################


##############################################
#c = np.array(1, 2, 3, 4) #YANLIŞ KULLANIM
c = np.array([1, 2, 3, 4]) #DOĞRU KULLANIM
##############################################


##############################################
d = np.array([(1.5, 2, 3), (4, 5, 6)])
"""
	bir ndarray içerisindeki elemanın en az
	bir tanesi bile float değerde olursa
	o ndarray değişkeninin tamamı
	float64 veri tipinde olur.
"""
##############################################


##############################################
e = np.array([1, 2], dtype=complex)
print(e, e.dtype)

"""
	int64 olan ndarray değişkeninin veri tipini
	değişkenin içerisinde dtype= ile tanımlayarak
	complex türündeki veri tipine dönüştürmüş olduk.
"""
##############################################


##############################################
f = np.array([1.0,2,3], dtype=int)
print(f, f.dtype)

"""
	Aslında f değişkeni bir float64 türünde veri barındırıyor
	ancak biz değişken içerisinde dtype = ile o değişken
	içerisindeki elemanları int64 veri tipine dönüştürdük.
"""
##############################################


##############################################
g = np.zeros( (3,4) )

"""
	Yukarda 3 satırlık ve 4 sütunluk bir dizi
	oluşturduk ve .zeros ile tüm içeriği
	float veri tipinde 0 lardan oluşan bir 
	değişken oluşturduk.
"""
##############################################


##############################################
h = np.ones((2, 3, 4), dtype=np.int16)

"""
	Buradaki önemli olan nokta .ones(x, y, z)
	parametrelerinden:
	x: kaç tane dizi oluşturulacağı
	y: kaç satırlık dizi olacağı
	z: kaç sütunluk dizi olacağını belirtir.

	dtpye = np.int16 ise numpy içerisindeki
	int16 veri tipine dönüştür demektir.

	İşlem sonunda dizinin tüm elemanlarının içeriği
	1 rakamlarından oluşmaktadır.
"""
##############################################


##############################################
i = np.empty( (2, 3))

"""
	.empty, ilk içeriği rastgele olan
	ve belleğin durumuna bağlı bir dizi oluşturur.
"""
##############################################



##############################################
j = np.arange(2, 30, 2)

"""
	Basit python range() fonksiyonu mantığı
	2 den 30 a kadar 2 şer 2 şer atla. :)
"""

k = np.arange(0, 2, 0.1)

"""
	0 dan 2 ye kadar 0.1 er şekilde atlayarak
	liste oluştur. :)
"""

##############################################


##############################################
from numpy import pi 

l = np.linspace(0, 2, 9)

"""
	Buradaki mantık ise şudur:
		0 dan 2 ye kadar (tüm reel sayılar)
		9 tane değer oluştur.
"""

m = np.linspace(0, 2*pi, 100)
print(m)


#n = np.sin(x)

##############################################

"""
Ayrıca Bakınız:
	array,
	zeros,
	zeros_like,
	ones,
	ones_like,
	empty,
	empty_like,
	arange,
	linspace,
	numpy.random.rand,
	numpy.random.randn,
	fromfunction,
	fromfile
"""


















































